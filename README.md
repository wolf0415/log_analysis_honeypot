# log_analysis_honeypot

## About 
* This project is about analyzing logs in the format of `json` and `log` captured from honeypot (`cowrie`)
* The logs are obtained through one of my friends from University. 
* This project will be doing the following process:
    * Collect data
    * Clean/Process data 
    * Analyze data 
    * Create visualization diagrams

## Disclaimer 
* Do not try to interact with the IP address listed in the logs. 